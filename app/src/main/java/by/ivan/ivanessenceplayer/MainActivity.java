package by.ivan.ivanessenceplayer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.app.FragmentTransaction;

import by.ivan.ivanessenceplayer.Fragments.MusicList;
import by.ivan.ivanessenceplayer.Fragments.PlayerFragment;
import by.ivan.ivanessenceplayer.Fragments.RadioFragment;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private ImageView radio;
    private ImageView songs;
    private RadioFragment radioFragment;
    private MusicList listFragment;
    private FragmentTransaction fTrans;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i(TAG, "onCreate: ");
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        radio = (ImageView) findViewById(R.id.radioFragment);
        songs = (ImageView) findViewById(R.id.songsFragment);
        radioFragment = new RadioFragment();
        listFragment = new MusicList();
        try {
            fTrans = getFragmentManager().beginTransaction();
            fTrans.replace(R.id.frgmCont, listFragment);
            fTrans.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    fTrans.remove(listFragment);
                    fTrans = getFragmentManager().beginTransaction();
                    fTrans.replace(R.id.frgmCont, radioFragment);
                    fTrans.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        songs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    fTrans.remove(radioFragment);
                    fTrans = getFragmentManager().beginTransaction();
                    fTrans.replace(R.id.frgmCont, listFragment);
                    fTrans.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

}
