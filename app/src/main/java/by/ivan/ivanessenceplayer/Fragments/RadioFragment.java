package by.ivan.ivanessenceplayer.Fragments;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TextView;

import by.ivan.ivanessenceplayer.R;

public class RadioFragment extends Fragment implements View.OnClickListener {

    public static final String TAG = "RadioFragment";
    public static final String NEUROFUNK = "http://79.111.119.111:8000/neurofunk";
    public static final String RECORDDNB = "http://air.radiorecord.ru:8102/ps_320";
    public static final String DIRTYLABDNB = "http://50.7.70.66:8693/stream?type=.mp3";
    public static final String RECORDHARDSTYLE = "http://air.radiorecord.ru:8102/teo_320";

    private TextView neurofunk;
    private TextView recordPS;
    private TextView dirtlabaudio;
    private TextView recordTeodor;
    private MediaPlayer mp;

    public RadioFragment() {
        mp = new MediaPlayer();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_radio, container, false);
        Log.i(TAG, "onCreateView: ");
        neurofunk = (TextView) view.findViewById(R.id.neurofunk);
        recordPS = (TextView) view.findViewById(R.id.recordDnB);
        dirtlabaudio = (TextView) view.findViewById(R.id.dirtlabaudio);
        recordTeodor = (TextView) view.findViewById(R.id.teodor);
        neurofunk.setOnClickListener(this);
        recordPS.setOnClickListener(this);
        dirtlabaudio.setOnClickListener(this);
        recordTeodor.setOnClickListener(this);

        TabHost tabHost = (TabHost) view.findViewById(R.id.tabHost);

        tabHost.setup();

        TabHost.TabSpec tabSpec = tabHost.newTabSpec("tag1");

        tabSpec.setContent(R.id.linearLayout);
        tabSpec.setIndicator("dNb Radio");
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec("tag2");
        tabSpec.setContent(R.id.linearLayout2);
        tabSpec.setIndicator("Club");
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec("tag3");
        tabSpec.setContent(R.id.linearLayout3);
        tabSpec.setIndicator("Other shit");
        tabHost.addTab(tabSpec);

        tabHost.setCurrentTab(0);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.neurofunk:
                Intent nfk = new Intent("playRadio");
                nfk.putExtra("streamUrl", NEUROFUNK);
                getActivity().sendBroadcast(nfk);
                break;
            case R.id.recordDnB:
                Intent recDnb = new Intent("playRadio");
                recDnb.putExtra("streamUrl", RECORDDNB);
                getActivity().sendBroadcast(recDnb);
                break;
            case R.id.dirtlabaudio:
                Intent dirtlabaudio = new Intent("playRadio");
                dirtlabaudio.putExtra("streamUrl", DIRTYLABDNB);
                getActivity().sendBroadcast(dirtlabaudio);
                break;
            case R.id.teodor:
                Intent recordTeodor = new Intent("playRadio");
                recordTeodor.putExtra("streamUrl", RECORDHARDSTYLE);
                getActivity().sendBroadcast(recordTeodor);
                break;
            default:
                break;
        }
    }
}
