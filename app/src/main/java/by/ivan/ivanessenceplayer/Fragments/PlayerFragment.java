package by.ivan.ivanessenceplayer.Fragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.IOException;

import by.ivan.ivanessenceplayer.R;

public class PlayerFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "PlayerFragment";
    private static final String MEDIA_PATH = new String("/sdcard/Download/");

    final MediaPlayer mp;
    private View view;
    private Button btnStart;
    private Button btnNext;
    private Button btnPrevious;
    private SeekBar trackBar;
    private TextView trackTiming;
    private boolean isPlaying;

    public PlayerFragment() {
        mp = new MediaPlayer();
    }

    private final BroadcastReceiver musicReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals("playTrack")) {
                String track = intent.getStringExtra("track");
                try {
                    boolean a = mp.isPlaying();
                    Log.i(TAG, String.valueOf(a));
                    mp.reset();
                    mp.setDataSource(MEDIA_PATH + track);
                    mp.prepare();
                    mp.start();
                    int duration = mp.getDuration();
                    trackBar.setMax(duration);
                    Log.i(TAG, String.valueOf(duration));
                    Timing timing = new Timing();
                    timing.execute();
                    btnStart.setBackgroundResource(R.drawable.pause);
                    isPlaying = true;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (action.equals("playRadio")) {
                final String streamUrl = intent.getStringExtra("streamUrl");
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            mp.reset();
                            mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
                            mp.setDataSource(streamUrl);
                            mp.prepare();
                            mp.start();
                            Message msg2 = new Message();
                            msg2.what = HANDLER_RADIO;
                            myHandler.sendMessage(msg2);
                            isPlaying = true;
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                thread.start();
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_player, container, false);
        trackBar = (SeekBar) view.findViewById(R.id.trackBar);
        btnStart = (Button) view.findViewById(R.id.btnStart);
        btnNext = (Button) view.findViewById(R.id.btnNext);
        btnPrevious = (Button) view.findViewById(R.id.btnPrevious);
        trackTiming = (TextView) view.findViewById(R.id.trackTiming);
        btnStart.setOnClickListener(this);
        IntentFilter musicFilter = new IntentFilter();
        musicFilter.addAction("playTrack");
        musicFilter.addAction("playRadio");
        getActivity().registerReceiver(this.musicReceiver, musicFilter);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnStart:
                if (isPlaying == true) {
                    mp.pause();
                    btnStart.setBackgroundResource(R.drawable.play);
                    isPlaying = false;
                } else {
                    mp.start();
                    isPlaying = true;
                    btnStart.setBackgroundResource(R.drawable.pause);
                }
                break;
            default:
                break;
        }
    }

    private static final int HANDLER_MESSAGE_TIMING = 1;
    private static final int HANDLER_MESSAGE_SEEKBAR = 2;
    private static final int HANDLER_RADIO = 3;

    Handler myHandler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case HANDLER_MESSAGE_TIMING: //change time it real-time
                    String a = String.valueOf(msg.obj);
                    trackTiming.setText(a);
                    break;
                case HANDLER_MESSAGE_SEEKBAR: //change bar in real-time
                    String ab = String.valueOf(msg.obj);
                    Integer b = Integer.valueOf(ab);
                    trackBar.setProgress(b);
                    break;
                case HANDLER_RADIO:
                    btnStart.setBackgroundResource(R.drawable.pause);
                    break;
                default:
                    break;
            }
        }
    };

    private class Timing extends AsyncTask<String, Void, String> {
        int pos = 0;
        int timing = 0;

        @Override
        protected String doInBackground(String... params) {
            while (mp.isPlaying()) {
                try {
                    Thread.sleep(1000);
                    pos = mp.getCurrentPosition();
                    Message msg = new Message();
                    msg.what = HANDLER_MESSAGE_SEEKBAR;
                    msg.obj = pos;
                    myHandler.sendMessage(msg);
                    timing++;
                    Message msg2 = new Message();
                    msg2.what = HANDLER_MESSAGE_TIMING;
                    msg2.obj = timing;
                    myHandler.sendMessage(msg2);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return "1";
        }
    }

}
