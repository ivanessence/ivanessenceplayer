package by.ivan.ivanessenceplayer.Fragments;


import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

import by.ivan.ivanessenceplayer.R;

public class MusicList extends ListFragment {

    private static final String MEDIA_PATH = new String("/sdcard/Download/");
    private List<String> songs = new ArrayList<String>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_music_list, container, false);
        updateSongList();
        return view;
    }

    public void updateSongList() {
        File home = new File(MEDIA_PATH);
        if (home.listFiles(new Mp3Filter()).length > 0) {
            for (File file : home.listFiles(new Mp3Filter())) {
                songs.add(file.getName());
            }

            ArrayAdapter<String> songList = new ArrayAdapter<String>(getActivity(),
                    R.layout.mytextview, songs);
            setListAdapter(songList);
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        int currentPosition = position;
        String a = songs.get(position);
        Intent trackIntent = new Intent("playTrack");
        trackIntent.putExtra("track", a);
        getActivity().sendBroadcast(trackIntent);
    }

    private class Mp3Filter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            return (name.endsWith(".mp3"));
        }
    }

}
